# DebConf 21 Lisbon Bid 

## License

The Metropolis theme is licensed under a [Creative Commons Attribution-ShareAlike
4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/). This
means that if you change the theme and re-distribute it, you *must* retain the
copyright notice header and license it under the same CC-BY-SA license. This
does not affect the presentation that you create with the theme.


[demo slides]: http://mirrors.ctan.org/macros/latex/contrib/beamer-contrib/themes/metropolis/demo/demo.pdf
[manual]: http://mirrors.ctan.org/macros/latex/contrib/beamer-contrib/themes/metropolis/doc/metropolistheme.pdf

## License (extended)

This theme was modified by Manuel Torrinha <manuel.torrinha@tecnico.ulisboa.pt>, to reflect
the image identity standards from Instituto Superior Técnico
